angular.module('victory.controllers').controller('AppController',function($ionicPlatform,$ionicLoading,$ionicViewService,SettingProvider,$rootScope,$http,$ionicNavBarDelegate,$ionicPopup,$scope,$state,AuthenticationService,UserService){

    var isPopupShouldShown = true;
    $rootScope.showPopup = function(data,buttons) {
        $scope.data = {
            message:data.message
        };
        console.log(isPopupShouldShown);
        if(isPopupShouldShown){
            var popup = $ionicPopup.show({
                template: '{{data.message}}',
                title:data.title,
                scope: $scope,
                buttons:buttons
            });
            popup.then(function(){
                isPopupShouldShown = true;
            });
            isPopupShouldShown = false;
        }

    };
    $rootScope.showAlert = function(msg,callback){
        console.log(isPopupShouldShown);
        if(isPopupShouldShown){
            var alertPopup = $ionicPopup.alert({
                title: 'ALERT',
                template: msg,
                okType :"button-calm"
            });
            alertPopup.then(function(res) {
                isPopupShouldShown = true;
                if(callback)
                    callback();
            });
            isPopupShouldShown = false;
        }
    }
    $rootScope.$on('hideLoading',function(e){
        $ionicLoading.hide();
    });
    $rootScope.$on('showLoading',function(e){
        $ionicLoading.show({
            content: '<i class="icon ion-loading-d"></i>',
            animation: 'fade-in',
            showBackdrop: true,
            showDelay: 200,
            duration:10000,
        });
    });
    $scope.$on('event:auth-loginRequired', function(e, rejection) {
        $state.go("app.login");

    });
    $scope.$on('event:auth-loginConfirmed', function() {
        AuthenticationService.getPermissions(window.localStorage.getItem("username")).success(function(data){
            UserService.setPermissions(data);
            $state.go("app.loading");
        })

    });
    $scope.$on('event:auth-logout-complete', function() {
        window.localStorage.removeItem("username");
        window.localStorage.removeItem("bod");
        $state.go('app.login', {}, {reload: true, inherit: false});

    });
    $rootScope.goBack = function() {
        $ionicNavBarDelegate.back();
    };
    $scope.logout = function() {
        // A confirm dialog
        $scope.showPopup({
            title:"SIGN OUT",
            message:"Your account will be logged out.<br/>Are you sure?"
        },
                         [{
                             text: 'OK',
                             type: 'button',
                             onTap: function(e) {
                                 AuthenticationService.logout();
                                 return true;
                             }
                         },
                          { text: 'Cancel', onTap: function(e) { return true; } }]);
    };
    $ionicPlatform.registerBackButtonAction(function () {
        if(!$ionicViewService.getBackView()){
            $rootScope.showPopup({
                title:"ALERT",
                message:"Are You Sure To Close Application ?"
            },[
                {
                    text: 'OK',
                    type: 'button-positive',
                    onTap: function() {
                        ionic.Platform.exitApp();
                        return true;
                    }
                },
                {
                    text: 'CANCEL',
                    onTap: function() {
                        return false;
                    }
                }
            ]);

        } else {
            $rootScope.goBack();
        }
    }, 100);
    $rootScope.parseDataToGrid = function(data,section,column){
        var result = [];
        var currentBlock = null;
        var currentRow = null;
        var currentBlockIndex = 0;
        var createRow = function(){
            var row = {};
            for(var i=0;i<section;i++){
                row[i] = [];
            }
            return row;
        }

        for(i in data){
            var d = data[i];
            //init grid;
            if(result.length == 0){
                result.push(createRow());
                currentRow = result[0];
                currentBlock = currentRow[0];
            }
            if(currentBlock.length < column){
                currentBlock.push(d);
            } else {
                if(currentRow[currentBlockIndex+1]){
                    currentBlockIndex++;
                    currentBlock = currentRow[currentBlockIndex];
                } else {
                    var row = createRow();
                    result.push(row);
                    currentRow = row;
                    currentBlock = row[0];
                    currentBlockIndex = 0;
                }
                currentBlock.push(d);
            }

        }
        return result;

    };
    $scope.changeSetting = function(){
        $scope.data = {
            host:SettingProvider.getSetting("host"),
            no_pos:SettingProvider.getSetting("no_pos")
        };
        $ionicPopup.show({
            template: 'HOST<input type="text" ng-model="data.host">NO_POS<input type="text" ng-model="data.no_pos">',
            title:"CHANGE SETTING",
            scope: $scope,
            buttons: [{
                text: 'SAVE',
                type: 'button',
                onTap: function(e) {
                    SettingProvider.setSetting("host",$scope.data.host);
                    SettingProvider.setSetting("no_pos", $scope.data.no_pos);
                    $http.defaults.headers.common['no_pos'] =  SettingProvider.getSetting("no_pos");
                    return true;
                }
            },
                      { text: 'Cancel', onTap: function(e) { return true; } }]
        });
    }
});