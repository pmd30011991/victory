angular.module('victory.controllers', []);
angular.module('victory.services', []);
angular.module('victory.directives',[]);
angular.module('victory.filters',[]);
angular.module('victory', ['ionic','monospaced.elastic','timer','victory.controllers','victory.services','victory.directives'])
.config(['$httpProvider',function($httpProvider){
    $httpProvider.defaults.headers.post['Content-Type'] = 'application/json';
    $httpProvider.defaults.headers.common['Content-Type'] = 'application/json';
    $httpProvider.interceptors.push(function($q, $rootScope) {
        var numLoadings = 0;
        return {
            'request': function(config) {
                config.timeout = 10000;
                numLoadings++;
                // Show loader
               /* if(numLoadings<2)
                    $rootScope.$broadcast("showLoading");*/
                return config || $q.when(config)
            },
            'response': function(response) {
                if ((--numLoadings) === 0) {
                    // Hide loader
                   /* $rootScope.$broadcast("hideLoading");*/
                }
                return response || $q.when(response);
            },
            'responseError': function(response) {
                if(response.status == 0 ){
                    $rootScope.showAlert("Request timeout, Please try again.");
                }
                if (!(--numLoadings)) {
                    // Hide loader
                    if(!window.navigator.onLine){
                        $rootScope.showPopup({
                            title:"ALERT",
                            message:"Your phone has no Network Connection.Please connect and try again."
                        },[{
                            text: 'OK',
                            type: 'button-calm',
                            onTap: function(e) {
                                return true;
                            }
                        }]);
                    }
                   /* $rootScope.$broadcast("hideLoading");*/
                }

                return $q.reject(response);
            }
        }
    });
}])
.config(function($stateProvider, $urlRouterProvider){
    $stateProvider
    .state('app', {
        url: '/app',
        abstract: true,
        templateUrl:'views/main.html',
        controller: 'AppController'
    })
    .state('app.login', {
        url: '/login',
        templateUrl:'views/login.html',
        controller: 'LoginController'
    })    
    .state('app.loading', {
        url: '/loading',
        templateUrl:'views/loading.html',
        controller: 'LoadingController'
    })
    .state('app.chooseTable', {
        url: '/choosetable',
        templateUrl:'views/chooseTable.html',
        controller: 'ChooseTableController'
    })
    .state('app.order', {
        url: '/order/:noTable/:nameTable/:token',
        templateUrl:'views/order.html',
        controller: 'OrderController'
    })
    $urlRouterProvider.otherwise('/app/login');
})
.run(function($ionicPlatform,SettingProvider) {
    $ionicPlatform.ready(function() {
        SettingProvider.init();
        if(window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
        if(window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
})
