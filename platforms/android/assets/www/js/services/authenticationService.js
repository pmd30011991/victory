angular.module('victory.services', ['http-auth-interceptor'])
.factory('AuthenticationService', function($rootScope,$http, authService,SettingProvider,$ionicPopup) {
    var service = {
        login: function(user) {
            $http.defaults.headers.common['no_pos'] =SettingProvider.getSetting("no_pos");
            if(!SettingProvider.getSetting("key") || SettingProvider.getSetting("key").toUpperCase() != CryptoJS.SHA1(device.uuid.toUpperCase().substr(0,10)+"99999").toString().substr(0,5).toUpperCase()){
                var scope = $rootScope.$new()
                scope.data = {
                    uuid:device.uuid.toUpperCase().substr(0,10),
                    key:SettingProvider.getSetting("key")
                };
                $ionicPopup.show({
                    template: 'Serial<input type="text" disabled ng-model="data.uuid">Key<input type="text" ng-model="data.key">',
                    title:"CHANGE SETTING",
                    scope: scope,
                    buttons: [{
                        text: 'SAVE',
                        type: 'button',
                        onTap: function(e) {
                            SettingProvider.setSetting("key",scope.data.key);
                            return true;
                        }
                    },
                              { text: 'Cancel', onTap: function(e) { return true; } }]
                });
            } else {
                // $http.defaults.headers.common['no_pos'] ="123";
                $http.get(SettingProvider.getSetting("api")+'/CheckLoginUser/?u='+user.username+'&p='+user.password)
                .success(function (data, status, headers, config) {
                    if(data == "0"){
                        $rootScope.showAlert("Login Fail");
                    } else if(data == "-1"){
                        $rootScope.showAlert("POS has not been registered on the system");
                    } 
                    else {
                        window.localStorage.setItem("username",user.username);
                        $http.defaults.headers.common['user_name'] = user.username;
                        $http.defaults.headers.common['no_pos'] =SettingProvider.getSetting("no_pos");
                        authService.loginConfirmed(user, function(config) {  // Step 2 & 3
                            window.localStorage.setItem("username",user.username);
                            $http.defaults.headers.common['user_name'] = user.username;
                            $http.defaults.headers.common['no_pos'] = SettingProvider.getSetting("no_pos");
                            return config;
                        });
                    }
                })
                .error(function (data, status, headers, config) {
                    $rootScope.$broadcast('event:auth-login-failed', status);
                });
            }
        },
        getPermissions: function(username){
            return $http.get(SettingProvider.getSetting("api")+'/GetPermissions/'+username);
        },
        logout: function(user) {
            delete $http.defaults.headers.common.Authorization;
            $rootScope.$broadcast('event:auth-logout-complete');

            /*$http.post('https://logout', {}, { ignoreAuthModule: true })
      .finally(function(data) {
        delete $http.defaults.headers.common.Authorization;
        $rootScope.$broadcast('event:auth-logout-complete');
      });	*/		
        },	
        loginCancelled: function() {
            authService.loginCancelled();
        }
    };
    return service;
})