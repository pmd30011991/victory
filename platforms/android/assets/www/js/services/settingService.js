angular.module('victory.services')
.provider('SettingProvider', function() {
    var that = this;
    this.host = "116.100.29.67:8000";
    this.no_pos = "04";
    this.refreshRate = 60000;
    this.key ="";
    var $filename = "config.json";
    var fail = function(error){
        alert(error);
    }
    var $getSettingFromFile = function(callback,error){
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,function(fileSystem){
            fileSystem.root.getFile($filename, null, function(fileEntry){
                fileEntry.file(function(file){
                    var reader = new FileReader();
                    reader.onloadend = function(evt) {
                        var data = JSON.parse(evt.target.result);
                        callback(data);
                    };
                    reader.readAsText(file);

                }, error);
            }, error);
        },error);
    }
    var $saveSettingToFile = function(){
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0,function(fileSystem){
            fileSystem.root.getFile($filename, {create: true, exclusive: false}, function(fileEntry){
                fileEntry.createWriter(function(writer){
                    writer.onwriteend = function(evt) {
                    };
                    var data = {
                        host:that.host,
                        no_pos: that.no_pos,
                        key: that.key
                    }
                    writer.write(JSON.stringify(data));
                }, fail);
            }, fail);
        },function(error){
        });
    }
    this.getSetting = function(name){
        if(name == "api"){
             return "http://"+that.host+"/TMHPOSService/Rest";
        }
        return that[name];
    };
    this.setSetting = function(name,value){
        that[name]= value;
        $saveSettingToFile();
    }
    this.init = function(){
        $getSettingFromFile(function(data){
            console.log(data);
            console.log(!!data.host && !!data.no_pos);
            if(!!data.host && !!data.no_pos){
                that.setSetting("host",data.host); 
                that.setSetting("no_pos",data.no_pos); 
                that.setSetting("key",data.key); 
            } else {
                that.setSetting("host",that.host); 
                that.setSetting("no_pos",that.no_pos);
                that.setSetting("key",that.key);
                $saveSettingToFile();
            }
        },function(er){
        });
    }
    this.$get = function() {
        return this;
    }
})