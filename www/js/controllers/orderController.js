angular.module('victory.controllers').controller('OrderController',function($ionicPopup,$ionicScrollDelegate,$rootScope,$scope,$ionicNavBarDelegate,$stateParams,$rootScope,DataService,TableService,OrderService,SystemConstant,UserService){

    // Initial Data 
    var menus = DataService.getData("MenuDtos");
    var largeMenu = DataService.getData("LargeMenuDtos");
    var smallMenu = DataService.getData("SmallMenuDtos");
    var token = $stateParams.token;
    $scope.memos = DataService.getData("Memos");
    var memoPopup = null;
    $scope.showMemos =function(menu){
        var scope = $scope.$new();
        scope.memos = $scope.memos;
        scope.menu = menu;
        memoPopup = $ionicPopup.show({
            templateUrl:"views/memos.html",
            title:"ADD MEMO",
            scope: scope,
            buttons:[
                { text: 'Close', onTap: function(e) { return true; } }

            ]
        });
    };
    
    $scope.addMemo = function(menu,memo){
        menu.memo = memo;
        if(memoPopup){
            memoPopup.close();
        }
    }
    $scope.title = $stateParams.nameTable;
    var categories = [];
    //$scope.orderData = [];
    $scope.oldMenus = [];
    $rootScope.goBack = function(){
        releaseTable().then(function(){
            $ionicNavBarDelegate.back();
        });

    }
    //mapping data
    //loop for creating category first
    for(c in largeMenu){
        var cat = largeMenu[c];
        cat.menus = [];
        //loop menu with smallMenu to push data into categories
        for(m in menus){
            var menu = menus[m];
            //loop small menu 
            for(sm in smallMenu){
                var small = smallMenu[sm];
                if(small.CdMenu == menu.CdMenu && cat.CdLarge == small.CdLarge){
                    cat.menus.push(menu);
                }
            }
        }
        //add to categories array
        categories.push(cat);

    }
    $scope.categories = categories;
    $scope.menus = [];

    /*var parseGridData = function(data,column){
        var result = {};
        var currentRow = 0;
        if(result.lenght ==0){
            result[0]=[];
        }
        for( i in data){
            var d = data[i];
            if(i%column ==0){
                currentRow++;
                result[currentRow]=[];
            }
            result[currentRow].push(d);
        }
        return result;


    }*/
    var uid = 1;
    var getUniqueId = function(){
        return uid++;
    }
    OrderService.GetOrderData($stateParams.noTable).success(function(data){
        if(data.OrderDDtos){
            //$scope.orderData = data.OrderDDtos;
            $scope.oldMenus = data.OrderDDtos;
            for(i in $scope.oldMenus){
                var menu = $scope.oldMenus[i];
                menu.uid = getUniqueId();
            }
        }
    })


    var getMenu = function(src,code,memo,uid){

        for(m in src){
            var menu = src[m];
            if((!uid && (menu.cd_menu == code || menu.CdMenu == code) && !menu.memo) || (menu.uid && menu.uid == uid)){
                return menu;
            }
        }

        return;
    }

    /* var intesection = function(addMenu,removeMenu){
        var src = angular.copy($scope.orderData, src);
        var add = angular.copy(addMenu, add);
        var remove = angular.copy(removeMenu, remove);
        for(a in add){
            var has = false;
            for(i in src){
                if(src[i].cd_menu == add[a].cd_menu){
                    has= true;
                    src[i].qty_order+=add[a].qty_order;
                    break;
                }
            }
            if(!has){
                src.push(add[a]);
            }
        }

        for(r in remove){
            for(i in src){
                if(remove[r].cd_menu == src[i].cd_menu){
                    src[i].qty_order-=remove[r].qty_order;
                }
            }
        }
        return src.filter(function(i){return i.qty_order>0});
    }*/
    $scope.newMenus = [];
    $scope.cancelMenus = [];
    $scope.addMenu = function(code,e){
        e.stopPropagation();
        var menu = getMenu($scope.newMenus,code,null);
        if(menu && !menu.memo){
            menu.qty_order++;
        } else {
            var menu = getMenu(menus,code);
            $scope.newMenus.push({
                cd_menu : menu.CdMenu,
                nm_menu : menu.MenuName,
                qty_order: 1,
                uid: getUniqueId()

            });
        }
        //animation
        var elem = $(e.srcElement).closest(".menu-item").clone();
        var parent = $(e.srcElement).closest('.col');
        elem.addClass("addElement");
        elem.css({
            'width':parent.width(),
            'height':parent.height(),
            'top':parent.offset().top+5,
            'left':parent.offset().left+5
        });
        $("body").append(elem);

        setTimeout(function(){
            elem.addClass("flyOut");
            //elem.css("top",$(document).height());
            var top = $(".pane").height();
            elem.css({
                "-webkit-transform": "translate3d(0, "+top+"px, 0) scale(0.3)",
                "-moz-transform": "translate3d(0, "+top+"px, 0) scale(0.3)",
                "transform": "translate3d(0, "+top+"px, 0) scale(0.3)"
            })
        },0,elem)

        elem.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',   
                 function(e) {

                     $(this).remove();

                 });

        //$scope.orderResult = intesection($scope.newMenus,$scope.cancelMenus);
    }
    $scope.removeMenu = function(code,uid,type){
        if(type=="new"){
            var menu = getMenu($scope.newMenus,code,null,uid);
            if(menu && menu.qty_order>0){
                menu.qty_order--;
                if(menu.qty_order==0){
                    $scope.newMenus = $scope.newMenus
                    .filter(function (el) {
                        return el.uid != menu.uid;
                    });
                }
            } 
        }else {
            if(UserService.getPermission(SystemConstant.PERMISSION.CANCEL_ORDER)){
                var oldMenu = getMenu($scope.oldMenus,code,null,uid);
                var menu = getMenu($scope.cancelMenus,code,null,uid);
                if(menu){
                    menu.qty_order++;
                } else {
                    $scope.cancelMenus.push({
                        cd_menu : oldMenu.cd_menu,
                        nm_menu : oldMenu.nm_menu,
                        qty_order: 1
                    });
                }
                if(oldMenu && oldMenu.qty_order>0){
                    oldMenu.qty_order--;
                    if(oldMenu.qty_order==0){
                        $scope.oldMenus = $scope.oldMenus
                        .filter(function (el) {
                            return el.uid != oldMenu.uid;
                        });
                    }
                }

            }
        }

        //$scope.orderResult = intesection($scope.newMenus,$scope.cancelMenus);
    }
    $scope.deleteMenu = function(code,uid,type){
        var currentList = [];
        if(type == "new"){

            var menu = getMenu($scope.newMenus,code,null,uid);
            currentList = $scope.newMenus;

        } else {
            if(UserService.getPermission(SystemConstant.PERMISSION.CANCEL_ORDER)){
                var menu = getMenu($scope.oldMenus,code,null,uid);
                currentList = $scope.oldMenus;
            }
        }
        if(currentList.length>0){
            for(var i in currentList){
                var item = currentList[i];
                if(item.uid == menu.uid){
                    currentList.splice(i,1);
                    if(type!="new"){
                        $scope.cancelMenus.push(item);
                    }
                    break;
                }
                //$scope.removeMenu(code,type);

            }
        }
    }
    var releaseTable = function(){
        return TableService.updateTableStatus($stateParams.noTable,0);
    }
    $scope.cancelOrder = function(){

    }
    $scope.printAgain = function(orderData,token){
        $rootScope.$broadcast("showLoading");
        OrderService.printMenu(orderData,token).success(function(code){
            $rootScope.$broadcast("hideLoading");
            switch(code){
                case SystemConstant.PRINT_RESPONSE.SUCCESS:
                    $rootScope.showAlert("Print Successful",function(){
                        $rootScope.goBack();
                    })
                    break; 
                case SystemConstant.PRINT_RESPONSE.TIMEOUT:
                    $rootScope.showPopup({
                        title:"ALERT",
                        message:"Order Successful,But There Is Have A Corruption When Trying To Printing Menu, Do You Want To Print Again?"
                    },[
                        {
                            text: 'TRY PRINT',
                            type: 'button button-calm',
                            onTap: function(e) {
                                $scope.printAgain(orderData,token);
                                return true;
                            }
                        },
                        { 
                            text: 'Cancel',
                            onTap: function(e) { 
                                $rootScope.goBack();
                                return true; 
                            } 
                        }
                    ])
                    break;
                case SystemConstant.PRINT_RESPONSE.FAIL:
                    $rootScope.showAlert("Print Fail, Please Try Again",function(){
                        $rootScope.goBack();
                    })
                    break;
            }
        })
    }
    $scope.submitOrder = function(){
        $rootScope.$broadcast("showLoading");
        var orderData = {
            NoTable:$stateParams.noTable,
            OldOrderDtos:$scope.oldMenus,
            CancelOrderDtos:$scope.cancelMenus,
            NewOrderDtos:$scope.newMenus
        }
        OrderService.updateOrder(orderData,token).success(function(code){
            $rootScope.$broadcast("hideLoading");
            switch(code){
                case SystemConstant.ORDER_RESPONSE.SUCCESS:
                    $rootScope.showAlert("Order Successful",function(){
                        $rootScope.goBack();
                    })
                    break; 
                case SystemConstant.ORDER_RESPONSE.SUCCESS_NO_PRINT:
                    $rootScope.showPopup({
                        title:"ALERT",
                        message:"Order Successful,But There Is Have A Corruption When Trying To Printing Menu, Do You Want To Print Again?"
                    },[
                        {
                            text: 'TRY PRINT',
                            type: 'button button-calm',
                            onTap: function(e) {
                                $scope.printAgain(orderData,token);
                                return true;
                            }
                        },
                        { 
                            text: 'Cancel',
                            onTap: function(e) { 
                                $rootScope.goBack();
                                return true; 
                            } 
                        }
                    ])
                    break;
                case SystemConstant.ORDER_RESPONSE.FAIL:
                    $rootScope.showAlert("Order Fail, Please Try Again",function(){
                        $rootScope.goBack();
                    })
                    break;
                case SystemConstant.ORDER_RESPONSE.TIMEOUT:
                    $rootScope.showAlert("Your Order Session Has Been Timeout, Please Try Again",function(){
                        $rootScope.goBack();
                    })
                    break;
            }
        })
    }
    $scope.loadMenus = function(index){
        var menu = $scope.categories[index].menus;
        $scope.selectedCategory = $scope.categories[index].CdLarge;

        $scope.menus = $rootScope.parseDataToGrid(menu,2,2);
        $ionicScrollDelegate.scrollTop(true);
    }
    //default
    $scope.loadMenus(0);

});