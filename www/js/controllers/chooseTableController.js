angular.module('victory.controllers').controller('ChooseTableController',function(SettingProvider,
                                                                                   SystemConstant,
                                                                                   UserService,
                                                                                   $interval,
                                                                                   $ionicScrollDelegate,
                                                                                   $ionicActionSheet,
                                                                                   $scope,$rootScope,
                                                                                   $state,
                                                                                   $ionicViewService,
                                                                                   $ionicNavBarDelegate,
                                                                                   TableService,
                                                                                   DataService){
    $ionicViewService.clearHistory();
    var getTables = function(noFloor){
        TableService.getTables(noFloor).success(function(data){
            $scope.tables = $rootScope.parseDataToGrid(data,2,2);
        });
    };
    $scope.editMode = {
        changeTable :false,
        mergeTable:false,
        selectedTable: null 
    }
    $scope.exitEditMode = function(opt){
        $scope.editMode.changeTable = false;
        $scope.editMode.mergeTable = false;
    }
    $scope.enterEditMode = function(table,opt){

        $scope.editMode[opt]= true;
        $scope.editMode.selectedTable= table;
    }
    $scope.floors = DataService.getData("FloorDtos");
    $scope.selectedFloor = TableService.selectedFloor || $scope.floors[0].noFloor;
    $scope.tables = getTables($scope.selectedFloor);
    $scope.refresh = function(){
        var elem = document.getElementById("button-refresh");
        angular.element(elem).html("");
        angular.element(elem).addClass("ion-looping icon");
        DataService.fetchData()
        .then(function(values){
            TableService.getTables($scope.selectedFloor).success(function(data){
                $scope.tables = $rootScope.parseDataToGrid(data,2,2);
                angular.element(elem).removeClass("ion-looping");
                angular.element(elem).removeClass("icon");
                angular.element(elem).html("REFRESH");
            });
        });

    }
    $scope.getTableStatusName = function(code){
        return TableService.getTableStatusNameByCode(code);
    }
    $scope.enterTable = function(noTable,nameTable){
        if($scope.editMode.changeTable){
            if(UserService.getPermission(SystemConstant.PERMISSION.CANCEL_ORDER)){
                TableService.changeTable($scope.editMode.selectedTable.NoTable,noTable).success(function(data){
                    $rootScope.showAlert("Successful to change table from "+$scope.editMode.selectedTable.NameTable+" to "+nameTable);
                    $scope.exitEditMode();
                    $scope.refresh();

                })
            }
        }else if($scope.editMode.mergeTable){
            if(UserService.getPermission(SystemConstant.PERMISSION.CANCEL_ORDER)){
                TableService.mergeTable($scope.editMode.selectedTable.NoTable,noTable).success(function(data){
                    $rootScope.showAlert("Successful to merge table from "+$scope.editMode.selectedTable.NameTable+" to "+nameTable);
                    $scope.exitEditMode();
                    $scope.refresh();

                })
            }
        }else{
            if(UserService.getPermission(SystemConstant.PERMISSION.ORDERED)){
                TableService.getTableStatusCode(noTable).success(function(data){
                    if(data.Status != 3){
                        TableService.updateTableStatus(noTable,1).success(function(data){
                            $state.go("app.order",{noTable:noTable,nameTable:nameTable,token:data.replace(/"/g,"")});
                        });
                    } else {
                        $rootScope.showAlert("Table is using, Please Choose another.");
                        $scope.refresh();
                    }
                })
            }
        }

    }
    $scope.changeFloor= function(floor){
        TableService.selectedFloor = floor;
        $scope.selectedFloor = floor;
        $ionicScrollDelegate.scrollTop(true);
        getTables(floor);
    };
    $interval(function() {
        $scope.refresh();
    }, SettingProvider.getSetting("refreshRate"));
    $scope.showSetting = function(floor,table){
        var hideSheet = $ionicActionSheet.show({
            buttons: [
                { text: 'Change Table' },
                { text: 'Merge Table' }
            ],
            cancelText: 'Cancel',
            cancel: function() {
                $scope.exitEditMode();
            },
            buttonClicked: function(index) {
                switch(index){
                    case 0: // Change 
                        $scope.enterEditMode(table,"changeTable");
                        console.log($scope.editMode.changeTable);
                        break;
                    case 1:
                        $scope.enterEditMode(table,"mergeTable");
                        console.log($scope.editMode.mergeTable);
                        break;
                }
                return true;
            }
        });
    }
});