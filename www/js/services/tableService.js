angular.module('victory.services')
.service('TableService', function($rootScope,$http,SettingProvider) {
    
    this.selectedFloor = null;
    this.selectedTable = null;
    this.getTables = function(noFloor){
        return $http.get(SettingProvider.getSetting("api")+"/GetListTableStatusByCodeFloor/"+noFloor);
    }
    
    this.getTableStatusNameByCode = function(code){
        switch(code) {
            case 0:
                return "Empty";
                break;
            case 1:
                return "Have Orderd"
                break;
            case 2:
                return "Draft Payment";
                break;
            case 3:
                return "Using";
                break;
        }
    }
    this.getTableStatusCode = function(table){
        return $http.get(SettingProvider.getSetting("api")+"/GetTableStatusByCodeTable/"+table);
    }
    this.updateTableStatus = function(table,status){
        return $http.post(SettingProvider.getSetting("api")+"/UpdateStatus/"+table+"/"+status);
    }
     this.changeTable = function(from,to){
        return $http.post(SettingProvider.getSetting("api")+"/ChangeTable/"+from+"/"+to);
    }
     this.mergeTable = function(from,to){
         return $http.post(SettingProvider.getSetting("api")+"/MergeTable/"+from+"/"+to);
     }
})