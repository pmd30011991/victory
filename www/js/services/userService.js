angular.module('victory.services')
.service('UserService', function($q,$rootScope,$http,SettingProvider) {
    var permissions = null;
    this.getPermissions = function(){
        return permissions;
    }
    this.setPermissions = function(p) {
        permissions = p;
    };
    this.getPermission = function(p){
        if(!permissions[p]){
            $rootScope.showAlert("You don't have permission for this operation");
        }
        return permissions[p];
    }
})