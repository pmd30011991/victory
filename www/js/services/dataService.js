angular.module('victory.services')
.service('DataService', function($q,$rootScope,$http,SettingProvider) {
    var data = {};
    var permissions = null;
    this.fetchData = function(type){
        var deferred = $q.defer();
        $http.get(SettingProvider.getSetting("api")+"/RestSynData/01").success(function(d){
            data = d;
            deferred.resolve(d);
        }).error(function(){
             deferred.reject("Da co loi xay ra khi ket noi server");
        });
        return deferred.promise;
    }
    this.getData = function(type){
        return data[type];
    }
    this.getPermissions = function(){
        return permissions;
    }
    this.setPermissions = function(p) {
        permissions = p;
    }
})