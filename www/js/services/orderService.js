angular.module('victory.services')
.service('OrderService', function($rootScope,$http,SettingProvider) {

    this.GetOrderData = function(code){
        return $http.get(SettingProvider.getSetting("api")+"/GetOrderDataByCodeTable/"+code);
    }
    this.updateOrder = function(orderData,token,isPrintAgain){
        var data = {
            dto:orderData
        }
        if(isPrintAgain){

        }else {
            return $http({
                method:"POST",
                url:SettingProvider.getSetting("api")+"/UpdateNewOrder/"+token,
                data:angular.toJson(data)
            });
        }
    }

    this.printMenu = function(orderData,token){
        var data = {
            dto:orderData
        }
        return $http({
            method:"POST",
            url:SettingProvider.getSetting("api")+"/PrintAgain/"+token,
            data:angular.toJson(data)
        });
    }
})