angular.module('victory.directives')
.directive('message', function($parse, $timeout) {
    return {
      restrict: 'E',
        replace:true,
        template:'<div class="message"></div>',
        scope:{
            type:"=",
            content:"="
        },
      link: function(scope, element, attrs, form) {
          scope.$on("showMessage",function(e,data){
              scope.type = data.type;
              scope.content = data.content;
          })
          scope.$watch("type",function(){
            element.attr("class","message "+scope.type);
          });
          scope.$watch("content",function(){
              if(!scope.content){
              element.addClass("ng-hide");
              } else {
                  element.removeClass("ng-hide");
              }
            element.html(scope.content);
          });
      }
    }
 
  });