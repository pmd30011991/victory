angular.module('victory').run(function($httpBackend){

    $httpBackend.whenPOST("/login.json").respond(function(method, url, data) {
        var user = angular.fromJson(data);
        if(user.username=="admin" && user.password=="admin"){
            var user = {
                username:"admin",
                bod:"123456"
            }
            return [200,user,{}];
        } else {
            return [401];
        }

    });
    $httpBackend.whenGET(/\/GetTableStatusByCodeTable\/[0-9]*/).respond(function(method, url, data) {
        var table = url.match(/GetTableStatusByCodeTable\/([0-9]*)/i)[1];
        if(table){
            if(table % 4 == 0 ){
                return [200,0,{}]
            } else if(table % 4 ==1 ) {
                return [200,1,{}];
            }else if(table % 4 ==2) {
                return [200,2,{}];
            }else{
                return [200,3,{}];
            }
        }
    });
    $httpBackend.whenGET(/\/GetListTableStatusByCodeFloor\/[0-9]*/).respond(function(method, url, data) {
        var noFloor = url.match(/GetListTableStatusByCodeFloor\/([0-9]*)/i)[1];
        var tables = [];
        if(noFloor){
            for(i=0 ; i<noFloor;i++){
                tables.push({NoTable:i,NameTable:"Table "+i,NoFloor:noFloor,Status:i%4});
            }
        }
        return [200,tables,{}];
    });
    $httpBackend.whenPOST(/\/UpdateStatus\/[0-9]*\/[0-9]*/).respond(function(method,url,data) {
        var table = url.match(/\/UpdateStatus\/([0-9]*)\/([0-9]*)/i)[1];
        var status = url.match(/\/UpdateStatus\/([0-9]*)\/([0-9]*)/i)[2];
        return [200];
    });
    $httpBackend.whenGET(/\/GetOrderDataByCodeTable\/[0-9]*/).respond(function(method, url, data) {
        var table = url.match(/GetOrderDataByCodeTable\/([0-9]*)/i)[1];
        var orders = {
            no_table:table,
            OrderDDtos:[]
        };
        if(table % 4 == 1 || table % 4 == 2 || table % 4 == 3){
            orders = {
                no_table:table,
                OrderDDtos:[
                    {cd_menu:1,nm_menu:"Menu 1",qty_order:1},
                    {cd_menu:2,nm_menu:"Menu 2",qty_order:4},
                    {cd_menu:3,nm_menu:"Menu 3",qty_order:5},
                ]
                    }
                    }
                    return [200,orders,{}];
            });
            $httpBackend.whenGET(/\/RestSynData\/(.*)/).respond(function(method, url, data) {

                var type = url.match(/RestSynData\/(.*)/i)[1];
                var result = [];
                switch(type){
                    case "FloorDto":
                        result = [
                            {NoFloor:1,NameFloor:"Tang 1",TypePrice:"A"},
                            {NoFloor:2,NameFloor:"Tang 2",TypePrice:"B"},
                            {NoFloor:3,NameFloor:"Tang 3",TypePrice:"C"},
                            {NoFloor:4,NameFloor:"Tang 4",TypePrice:"D"},
                            {NoFloor:5,NameFloor:"Tang 5",TypePrice:"E"}
                        ];
                        break;
                    case "LargeMenuDto":
                        result = [
                            {CdKind:1,CdLarge:1,NameLarge:"Category 1",Position:1},
                            {CdKind:2,CdLarge:2,NameLarge:"Category 2",Position:2},
                            {CdKind:3,CdLarge:3,NameLarge:"Category 3",Position:3},
                            {CdKind:4,CdLarge:4,NameLarge:"Category 4",Position:4},
                            {CdKind:5,CdLarge:5,NameLarge:"Category 5",Position:5}
                        ];
                        break;
                    case "MenuDto":
                        result = [
                            {CdMenu:1,MenuName:"Menu 1"},
                            {CdMenu:2,MenuName:"Menu 2"},
                            {CdMenu:3,MenuName:"Menu 3"},
                            {CdMenu:4,MenuName:"Menu 4"},
                            {CdMenu:5,MenuName:"Menu 5"}
                        ];
                        break;
                    case "SmallMenuDto":
                        result = [
                            {CdKind:1,CdMenu:1,CdLarge:1,Position:1},
                            {CdKind:6,CdMenu:1,CdLarge:2,Position:1},
                            {CdKind:7,CdMenu:2,CdLarge:2,Position:2},
                            {CdKind:6,CdMenu:1,CdLarge:3,Position:1},
                            {CdKind:7,CdMenu:2,CdLarge:3,Position:2},
                            {CdKind:8,CdMenu:3,CdLarge:3,Position:3},
                            {CdKind:6,CdMenu:1,CdLarge:4,Position:1},
                            {CdKind:7,CdMenu:2,CdLarge:4,Position:2},
                            {CdKind:8,CdMenu:3,CdLarge:4,Position:3},
                            {CdKind:9,CdMenu:4,CdLarge:4,Position:4},
                            {CdKind:6,CdMenu:1,CdLarge:5,Position:1},
                            {CdKind:7,CdMenu:2,CdLarge:5,Position:2},
                            {CdKind:8,CdMenu:3,CdLarge:5,Position:3},
                            {CdKind:9,CdMenu:4,CdLarge:5,Position:4},
                            {CdKind:1,CdMenu:5,CdLarge:5,Position:5}
                        ];
                        break;

                }
                return [200,result,{}];
            });
            $httpBackend.whenGET(/(.*)\.html/).passThrough();

            $httpBackend.whenPOST("/UpdateNewOrder").respond(function(method, url, data) {
                return [200];
            });
        })