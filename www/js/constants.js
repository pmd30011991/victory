angular.module('victory').constant("SystemConstant",{
    ORDER_RESPONSE: {
        SUCCESS: "0",
        SUCCESS_NO_PRINT: "1",
        FAIL: "2",
        TIMEOUT: "3"
    },
    PRINT_RESPONSE:{
        SUCCESS:"0",
        TIMEOUT:"2",
        FAIL:"3",
        
    },
    PERMISSION:{
        CANCEL_ORDER:"_CancelOrder",
        CLOSE_POS:"_ClosePOS",
        DISCOUNT:"_DiscountRCP",
        EDIT_TABLE:"_EditableRcp",
        INCOMS_COST:"_IncomsCost",
        ORDERED:"_Ordered",
        PAYMENT:"_Payment",
        SERVICE:"_Service"
    }
})